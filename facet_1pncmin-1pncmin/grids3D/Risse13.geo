Merge "Risse13.brep";

// Physical domain definitions
Physical Volume(2) = {2};
Characteristic Length{ PointsOf{Volume{2};} } = 0.08;
Physical Volume(1) = {1};
Characteristic Length{ PointsOf{Volume{1};} } = 0.08;

// Physical entity definitions
Physical Surface(20) = {27};
Characteristic Length{ PointsOf{Surface{27};} } = 0.008;
Physical Surface(18) = {26};
Characteristic Length{ PointsOf{Surface{26};} } = 0.008;
Physical Surface(17) = {25};
Characteristic Length{ PointsOf{Surface{25};} } = 0.008;
Physical Surface(16) = {24};
Characteristic Length{ PointsOf{Surface{24};} } = 0.008;
Physical Surface(3) = {12};
Characteristic Length{ PointsOf{Surface{12};} } = 0.008;
Physical Surface(2) = {11};
Characteristic Length{ PointsOf{Surface{11};} } = 0.008;
Physical Surface(19) = {9, 10};
Characteristic Length{ PointsOf{Surface{9, 10};} } = 0.008;
Physical Surface(1) = {4};
Characteristic Length{ PointsOf{Surface{4};} } = 0.008;
Physical Surface(4) = {7};
Characteristic Length{ PointsOf{Surface{7};} } = 0.008;
Physical Surface(11) = {5, 6, 8};
Characteristic Length{ PointsOf{Surface{5, 6, 8};} } = 0.008;
Physical Surface(5) = {13};
Characteristic Length{ PointsOf{Surface{13};} } = 0.008;
Physical Surface(8) = {14};
Characteristic Length{ PointsOf{Surface{14};} } = 0.008;
Physical Surface(10) = {15};
Characteristic Length{ PointsOf{Surface{15};} } = 0.008;
Physical Surface(15) = {16, 17};
Characteristic Length{ PointsOf{Surface{16, 17};} } = 0.008;
Physical Surface(6) = {18};
Characteristic Length{ PointsOf{Surface{18};} } = 0.008;
Physical Surface(12) = {19};
Characteristic Length{ PointsOf{Surface{19};} } = 0.008;
Physical Surface(7) = {20};
Characteristic Length{ PointsOf{Surface{20};} } = 0.008;
Physical Surface(9) = {21};
Characteristic Length{ PointsOf{Surface{21};} } = 0.008;
Physical Surface(13) = {22};
Characteristic Length{ PointsOf{Surface{22};} } = 0.008;
Physical Surface(14) = {23};
Characteristic Length{ PointsOf{Surface{23};} } = 0.008;

// Physical entity intersections definitions
Physical Line(8) = {77};
Physical Line(7) = {69};
Physical Line(3) = {49};
Physical Line(13) = {39};
Physical Line(5) = {40};
