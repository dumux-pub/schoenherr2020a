Merge "Risse8.brep";

// Physical domain definitions
Physical Volume(2) = {2};
Characteristic Length{ PointsOf{Volume{2};} } = 0.08;
Physical Volume(1) = {1};
Characteristic Length{ PointsOf{Volume{1};} } = 0.08;

// Physical entity definitions
Physical Surface(13) = {35};
Characteristic Length{ PointsOf{Surface{35};} } = 0.008;
Physical Surface(11) = {34};
Characteristic Length{ PointsOf{Surface{34};} } = 0.008;
Physical Surface(12) = {33};
Characteristic Length{ PointsOf{Surface{33};} } = 0.008;
Physical Surface(5) = {32};
Characteristic Length{ PointsOf{Surface{32};} } = 0.008;
Physical Surface(7) = {13, 14};
Characteristic Length{ PointsOf{Surface{13, 14};} } = 0.008;
Physical Surface(6) = {11, 12, 16};
Characteristic Length{ PointsOf{Surface{11, 12, 16};} } = 0.008;
Physical Surface(2) = {9, 10};
Characteristic Length{ PointsOf{Surface{9, 10};} } = 0.008;
Physical Surface(19) = {17};
Characteristic Length{ PointsOf{Surface{17};} } = 0.008;
Physical Surface(1) = {5};
Characteristic Length{ PointsOf{Surface{5};} } = 0.008;
Physical Surface(18) = {28, 29};
Characteristic Length{ PointsOf{Surface{28, 29};} } = 0.008;
Physical Surface(17) = {8};
Characteristic Length{ PointsOf{Surface{8};} } = 0.008;
Physical Surface(10) = {6, 7};
Characteristic Length{ PointsOf{Surface{6, 7};} } = 0.008;
Physical Surface(9) = {15};
Characteristic Length{ PointsOf{Surface{15};} } = 0.008;
Physical Surface(14) = {18};
Characteristic Length{ PointsOf{Surface{18};} } = 0.008;
Physical Surface(15) = {19, 20};
Characteristic Length{ PointsOf{Surface{19, 20};} } = 0.008;
Physical Surface(16) = {21, 22};
Characteristic Length{ PointsOf{Surface{21, 22};} } = 0.008;
Physical Surface(3) = {23};
Characteristic Length{ PointsOf{Surface{23};} } = 0.008;
Physical Surface(20) = {30, 31};
Characteristic Length{ PointsOf{Surface{30, 31};} } = 0.008;
Physical Surface(4) = {24, 25};
Characteristic Length{ PointsOf{Surface{24, 25};} } = 0.008;
Physical Surface(8) = {26, 27};
Characteristic Length{ PointsOf{Surface{26, 27};} } = 0.008;

// Physical entity intersections definitions
Physical Line(9) = {139};
Physical Line(31) = {123};
Physical Line(16) = {116};
Physical Line(28) = {99};
Physical Line(27) = {98};
Physical Line(17) = {97};
Physical Line(2) = {88};
Physical Line(4) = {89};
Physical Line(21) = {66};
Physical Line(3) = {90};
Physical Line(37) = {59};
Physical Line(20) = {68};
Physical Line(22) = {67};
Physical Line(5) = {69};
Physical Line(15) = {115};
Physical Line(32) = {77};
