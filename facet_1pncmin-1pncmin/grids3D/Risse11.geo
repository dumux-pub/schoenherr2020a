Merge "Risse11.brep";

// Physical domain definitions
Physical Volume(2) = {2};
Characteristic Length{ PointsOf{Volume{2};} } = 0.08;
Physical Volume(1) = {1};
Characteristic Length{ PointsOf{Volume{1};} } = 0.08;

// Physical entity definitions
Physical Surface(19) = {32};
Characteristic Length{ PointsOf{Surface{32};} } = 0.008;
Physical Surface(18) = {31};
Characteristic Length{ PointsOf{Surface{31};} } = 0.008;
Physical Surface(17) = {30};
Characteristic Length{ PointsOf{Surface{30};} } = 0.008;
Physical Surface(16) = {29};
Characteristic Length{ PointsOf{Surface{29};} } = 0.008;
Physical Surface(6) = {10, 11};
Characteristic Length{ PointsOf{Surface{10, 11};} } = 0.008;
Physical Surface(13) = {9, 12};
Characteristic Length{ PointsOf{Surface{9, 12};} } = 0.008;
Physical Surface(3) = {8};
Characteristic Length{ PointsOf{Surface{8};} } = 0.008;
Physical Surface(20) = {23};
Characteristic Length{ PointsOf{Surface{23};} } = 0.008;
Physical Surface(7) = {7};
Characteristic Length{ PointsOf{Surface{7};} } = 0.008;
Physical Surface(1) = {5};
Characteristic Length{ PointsOf{Surface{5};} } = 0.008;
Physical Surface(2) = {6};
Characteristic Length{ PointsOf{Surface{6};} } = 0.008;
Physical Surface(9) = {13};
Characteristic Length{ PointsOf{Surface{13};} } = 0.008;
Physical Surface(15) = {14, 15, 17};
Characteristic Length{ PointsOf{Surface{14, 15, 17};} } = 0.008;
Physical Surface(4) = {16, 22};
Characteristic Length{ PointsOf{Surface{16, 22};} } = 0.008;
Physical Surface(5) = {18, 19};
Characteristic Length{ PointsOf{Surface{18, 19};} } = 0.008;
Physical Surface(8) = {20, 21};
Characteristic Length{ PointsOf{Surface{20, 21};} } = 0.008;
Physical Surface(11) = {24};
Characteristic Length{ PointsOf{Surface{24};} } = 0.008;
Physical Surface(12) = {25};
Characteristic Length{ PointsOf{Surface{25};} } = 0.008;
Physical Surface(10) = {26, 27};
Characteristic Length{ PointsOf{Surface{26, 27};} } = 0.008;
Physical Surface(14) = {28};
Characteristic Length{ PointsOf{Surface{28};} } = 0.008;

// Physical entity intersections definitions
Physical Line(8) = {92};
Physical Line(9) = {91};
Physical Line(6) = {85};
Physical Line(17) = {83};
Physical Line(3) = {69};
Physical Line(24) = {78};
Physical Line(22) = {110};
Physical Line(5) = {64};
Physical Line(15) = {71};
Physical Line(16) = {79};
Physical Line(18) = {82};
