Merge "Risse9.brep";

// Physical domain definitions
Physical Volume(2) = {2};
Characteristic Length{ PointsOf{Volume{2};} } = 0.08;
Physical Volume(1) = {1};
Characteristic Length{ PointsOf{Volume{1};} } = 0.08;

// Physical entity definitions
Physical Surface(20) = {29};
Characteristic Length{ PointsOf{Surface{29};} } = 0.008;
Physical Surface(18) = {28};
Characteristic Length{ PointsOf{Surface{28};} } = 0.008;
Physical Surface(17) = {27};
Characteristic Length{ PointsOf{Surface{27};} } = 0.008;
Physical Surface(12) = {26};
Characteristic Length{ PointsOf{Surface{26};} } = 0.008;
Physical Surface(6) = {12, 18};
Characteristic Length{ PointsOf{Surface{12, 18};} } = 0.008;
Physical Surface(2) = {10, 11};
Characteristic Length{ PointsOf{Surface{10, 11};} } = 0.008;
Physical Surface(19) = {21};
Characteristic Length{ PointsOf{Surface{21};} } = 0.008;
Physical Surface(16) = {9, 13};
Characteristic Length{ PointsOf{Surface{9, 13};} } = 0.008;
Physical Surface(11) = {7, 8};
Characteristic Length{ PointsOf{Surface{7, 8};} } = 0.008;
Physical Surface(1) = {4, 5};
Characteristic Length{ PointsOf{Surface{4, 5};} } = 0.008;
Physical Surface(15) = {6};
Characteristic Length{ PointsOf{Surface{6};} } = 0.008;
Physical Surface(3) = {14};
Characteristic Length{ PointsOf{Surface{14};} } = 0.008;
Physical Surface(5) = {15};
Characteristic Length{ PointsOf{Surface{15};} } = 0.008;
Physical Surface(14) = {16, 17};
Characteristic Length{ PointsOf{Surface{16, 17};} } = 0.008;
Physical Surface(13) = {19};
Characteristic Length{ PointsOf{Surface{19};} } = 0.008;
Physical Surface(7) = {20};
Characteristic Length{ PointsOf{Surface{20};} } = 0.008;
Physical Surface(4) = {22};
Characteristic Length{ PointsOf{Surface{22};} } = 0.008;
Physical Surface(8) = {23};
Characteristic Length{ PointsOf{Surface{23};} } = 0.008;
Physical Surface(9) = {24};
Characteristic Length{ PointsOf{Surface{24};} } = 0.008;
Physical Surface(10) = {25};
Characteristic Length{ PointsOf{Surface{25};} } = 0.008;

// Physical entity intersections definitions
Physical Line(23) = {78};
Physical Line(10) = {77};
Physical Line(22) = {71};
Physical Line(9) = {70};
Physical Line(15) = {45};
Physical Line(14) = {44};
Physical Line(7) = {60};
Physical Line(21) = {64};
Physical Line(4) = {49};
Physical Line(1) = {100};
Physical Line(18) = {54};
Physical Line(2) = {105};
Physical Line(19) = {55};
