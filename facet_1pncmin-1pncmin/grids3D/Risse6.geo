Merge "Risse6.brep";

// Physical domain definitions
Physical Volume(2) = {2};
Characteristic Length{ PointsOf{Volume{2};} } = 0.1;
Physical Volume(1) = {1};
Characteristic Length{ PointsOf{Volume{1};} } = 0.1;

// Physical entity definitions
Physical Surface(4) = {17};
Characteristic Length{ PointsOf{Surface{17};} } = 0.01;
Physical Surface(8) = {16};
Characteristic Length{ PointsOf{Surface{16};} } = 0.01;
Physical Surface(7) = {15};
Characteristic Length{ PointsOf{Surface{15};} } = 0.01;
Physical Surface(2) = {13, 14};
Characteristic Length{ PointsOf{Surface{13, 14};} } = 0.01;
Physical Surface(6) = {6, 10};
Characteristic Length{ PointsOf{Surface{6, 10};} } = 0.01;
Physical Surface(1) = {4, 5};
Characteristic Length{ PointsOf{Surface{4, 5};} } = 0.01;
Physical Surface(9) = {7};
Characteristic Length{ PointsOf{Surface{7};} } = 0.01;
Physical Surface(10) = {8, 12};
Characteristic Length{ PointsOf{Surface{8, 12};} } = 0.01;
Physical Surface(3) = {11};
Characteristic Length{ PointsOf{Surface{11};} } = 0.01;
Physical Surface(5) = {9};
Characteristic Length{ PointsOf{Surface{9};} } = 0.01;

// Physical entity intersections definitions
Physical Line(3) = {76};
Physical Line(14) = {68};
Physical Line(15) = {56};
Physical Line(1) = {81};
Physical Line(18) = {51};
Physical Line(4) = {36};
Physical Line(11) = {34};
Physical Line(12) = {37};
Physical Line(2) = {72};
Physical Line(19) = {40};
Physical Line(5) = {50};
Physical Line(16) = {44};
