Merge "Risse10.brep";

// Physical domain definitions
Physical Volume(2) = {3};
Characteristic Length{ PointsOf{Volume{3};} } = 0.1;
Physical Volume(1) = {1, 2};
Characteristic Length{ PointsOf{Volume{1, 2};} } = 0.1;

// Physical entity definitions
Physical Surface(1) = {4};
Characteristic Length{ PointsOf{Surface{4};} } = 0.01;
