meshSize = 0.5;

r1 = 0.055/2;
h1 = 0.08;

r2 = 0.055/2;
h2 = 0.02;

SetFactory("OpenCASCADE");
Cylinder(1) = {0, 0, 0, 0, 0, h1, r1, 2*Pi};
Cylinder(2) = {0, 0, h1, 0, 0, h2, r2, 2*Pi};
BooleanFragments{ Volume{1, 2}; Delete; }{ Volume{1, 2}; Delete; }

Characteristic Length{ PointsOf{ Volume{:}; } } = meshSize;
